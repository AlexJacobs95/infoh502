#version 330 core
layout (location = 0) in vec3 position;
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
out vec4 myColor;

void main() {
  gl_Position = P*V*M*vec4(position.xyz, 1.0);
  myColor = vec4(0.0f, 0.5f, 0.3f, 0.0f); // green color
}
