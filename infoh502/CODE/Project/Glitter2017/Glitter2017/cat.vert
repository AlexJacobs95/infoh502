#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 itexCoord;
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
out vec2 texCoord;

void main(){
	gl_Position = P*V*M*vec4(position.xyz, 1.0f);
	texCoord = vec2(itexCoord.x, 1.0 - itexCoord.y);
}
