#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 itexCoord;

uniform mat4 V;
uniform mat4 P;
uniform mat4 M;

out vec3 FragPos;
out vec3 Normal;
//out vec2 textCoord;


void main()
{
    FragPos = vec3(M * vec4(position, 1.0));
    Normal = vec3(inverse(transpose(M))*vec4(aNormal, 0));
    //texCoord = vec2(itexCoord.x, 1.0 - itexCoord.y); // pass the texture coordinates to the fragment shader

    gl_Position = P * V * vec4(FragPos, 1.0);
}
