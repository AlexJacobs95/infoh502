#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 itexCoord;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

out vec2 texCoord;
out vec3 FragPos;
out vec3 Normal;

void main() {
    FragPos = vec3(M * vec4(position, 1.0));
    Normal = vec3(inverse(transpose(M))*vec4(aNormal, 0));
	texCoord = vec2(itexCoord.x, itexCoord.y);

	gl_Position = P * V * vec4(FragPos, 1.0);
}
