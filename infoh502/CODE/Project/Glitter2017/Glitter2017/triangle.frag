#version 330 core
in vec3 myColor;

out vec3 color;

void main() {
	color = myColor;
}
