#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 col;
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
out vec3 myColor;

void main() {
	gl_Position = P*V*M*vec4(position.x+0.1f, position.y, position.z, 1.0f);
	myColor = col;
}
