// Local Headers
#include "glitter.hpp"

#define GLM_ENABLE_EXPERIMENTAL
// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Camera.hpp"
#include "Model.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/glm.hpp>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include "Shader.hpp"


// Callbacks
static bool keys[1024]; // is a key pressed or not ?
						// External static callback
						// Is called whenever a key is pressed/released via GLFW
static void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/);
static void mouse_button_callback(GLFWwindow* /*window*/, int button, int action, int /*mods*/);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double /*xoffset*/, double yoffset);
GLuint createTriangleVAO(void);
GLuint createAxes(void);
GLuint createCubeVAO(void);
GLuint createSquareVAO(void);
GLuint mapTexture(const char* filename);
void Do_Movement();
void showFPS(void);
Camera camera(glm::vec3(0.0f, 0.0f, 0.0f));
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;
float deltaTime = 0.0f;


int main(int argc, char * argv[]) {

    // Load GLFW and Create a Window
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    auto mWindow = glfwCreateWindow(800, 800, "INFO-H-502 - Team 1", nullptr, nullptr);
	//auto mWindow = glfwCreateWindow(glfwGetVideoMode(glfwGetPrimaryMonitor())->width, glfwGetVideoMode(glfwGetPrimaryMonitor())->height, "INFO-H-502 - Team 1", nullptr, nullptr);
	;
    // Check for Valid Context
    if (mWindow == nullptr) {
        fprintf(stderr, "Failed to Create OpenGL Context");
        return EXIT_FAILURE;
    }

    // Create Context and Load OpenGL Functions
    glfwMakeContextCurrent(mWindow);
    gladLoadGL();
    fprintf(stderr, "OpenGL %s\n", glGetString(GL_VERSION));

	// Set the required callback functions
	glfwSetKeyCallback(mWindow, key_callback);
	glfwSetCursorPosCallback(mWindow, mouse_callback);
	glfwSetMouseButtonCallback(mWindow, mouse_button_callback);
	glfwSetScrollCallback(mWindow, scroll_callback);

	// Init shader
	/*Shader shaderProgram("triangle.vert", "triangle.frag");
	shaderProgram.compile();*/

	Shader shaderProgram("texture.vert", "texture.frag");
	shaderProgram.compile();

	/*Shader lightShader("diffuseLight.vert", "diffuseLight.frag");
	lightShader.compile();*/

	/*Shader shaderProgram("bunny.vert", "bunny.frag");
	shaderProgram.compile();*/

	// Change Viewport
	int width, height;
	glfwGetFramebufferSize(mWindow, &width, &height);
	glViewport(0, 0, width, height);
	/*GLuint VAO = createTriangleVAO();
	GLuint VAO2 = createAxes();
	GLuint VAO_CUBE = createCubeVAO();
	GLuint VAO_SQ = createSquareVAO();*/
	GLuint texture1 = mapTexture("Texture_01_DiffuseMap.jpg");
	//GLuint texture2 = mapTexture("Texture_02_Bump.jpg");
	//GLuint texture2 = mapTexture("F16t.bmp");
	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 Projection = glm::perspective(glm::radians(45.0f), (float)width / (float)height, 0.1f, 100.0f);



	// Or, for an ortho camera :
	//glm::mat4 Projection = glm::ortho(-10.0f,10.0f,-10.0f,10.0f,0.0f,100.0f); // In world coordinates*/

	// Camera matrix
	glm::mat4 View = camera.GetViewMatrix();
	/*glm::mat4 View = glm::lookAt(
		glm::vec3(4, 3, 3), // Camera is at (4,3,3), in World Space
		glm::vec3(0, 0, 0), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);*/

	glm::mat4 myScalingMatrix = glm::scale(glm::vec3(10.0f, 10.0f, 10.0f));

	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 model = glm::mat4(1.0f)*myScalingMatrix;
	
	
	
	glEnable(GL_DEPTH_TEST);
	// Rendering Loop
	Model bunnyModel = Model("Adidas_Telstar.obj");
	glm::vec3 lightPos(0.0f, 00.0f, 50.0f);

	while (glfwWindowShouldClose(mWindow) == false) {
		showFPS();
		Do_Movement();
		float time = glfwGetTime();
		glm::mat4 View = camera.GetViewMatrix();
		glm::mat4 Projection = glm::perspective(camera.Zoom, (float)mWidth / (float)mHeight, 0.1f, 1000.0f);
		//glm::mat4 mvp = Projection * View * model; // Remember, matrix multiplication is the other way around
		//glm::mat4 vp = Projection * View; // Remember, matrix multiplication is the other way around
		// Background Fill Color
		glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// triangle
		shaderProgram.use();

		
		// Send our transformation to the currently bound shader, in the "MVP" uniform
		// This is done in the main loop since each model will have a different MVP matrix (At least for the M part)
		//glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture1);
		shaderProgram.setInteger("myTexture1", 0);
		/*glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);
		shaderProgram.setInteger("myTexture2", 1);*/
		//model = glm::rotate(model, time / 10000.0f, glm::vec3(0, 1, 0));
		shaderProgram.setMatrix4("M", model);
		shaderProgram.setMatrix4("V", View);
		shaderProgram.setMatrix4("P", Projection);
		shaderProgram.setVector3f("lightColor", 1.0f, 1.0f, 1.0f);
		shaderProgram.setVector3f("lightPos", lightPos);
		shaderProgram.setVector3f("viewPos", camera.Position);
		bunnyModel.Draw(shaderProgram);

		/*
		lightShader.use();
		lightShader.setMatrix4("M", model);
		lightShader.setMatrix4("V", View);
		lightShader.setMatrix4("P", Projection);
		lightShader.setVector3f("objectColor", 0.0f, 0.5f, 0.3f);
		lightShader.setVector3f("lightColor", 1.0f, 1.0f, 1.0f);
		lightShader.setVector3f("lightPos", lightPos);
		bunnyModel.Draw(lightShader);*/

		/*glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glBindVertexArray(0);

		glBindVertexArray(VAO2);
		glDrawElements(GL_LINES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);*/
		

		/*glBindVertexArray(VAO_CUBE);
		glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);*/

		/*glBindVertexArray(VAO_TR);
		glDrawElements(GL_LINES, 24, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);*/

        // Flip Buffers and Draw
        glfwSwapBuffers(mWindow);
        glfwPollEvents();
    }
	glfwTerminate();
    return EXIT_SUCCESS;
}

GLuint createTriangleVAO(void) {
	GLuint VAO, VBO;
	/*GLfloat vertices[] = {
		0.0f, 0.0f, 0.0f,  
		0.5f, 0.5f, 0.0f,
		0.0f, 0.5f, 0.0f }; */
	GLfloat vertices[] = {
		0.0f, 0.0f, 0.0f, 1.0, 0.0f, 0.0f, //add color for vertice 
		0.5f, 0.5f, 0.0f, 0.0, 1.0f, 0.0f,
		0.0f, 0.5f, 0.0f, 0.0, 0.0f, 1.0f };
	glGenVertexArrays(1, &VAO);
	// 1. Bind Vertex Array Object
	glBindVertexArray(VAO);
	// 2. Copy our vertices array in a buffer for OpenGL to use
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	// 3. Then set our vertex attributes pointers
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0); //color according to position
	//glEnableVertexAttribArray(1);
	//4. Unbind the VAO
	glBindVertexArray(0);
	// 5. (Game Loop) Draw the object in while
	return VAO;
}

GLuint createSquareVAO(void) {
	GLuint VAO, VBO, EBO;
	GLfloat vertices[] = {
		0.0f, 0.0f, 0.0f, // 0
		0.0f, 1.0f, 0.0f, // 1
		1.0f, 0.0f, 0.0f, // 2
		1.0f, 1.0f, 0.0f, // 3
		};
	GLuint indices[] = { 0,1,3,
						 0,2,3 };
	glGenVertexArrays(1, &VAO);
	// 1. Bind Vertex Array Object
	glBindVertexArray(VAO);
	// 2. Copy our vertices array in a buffer for OpenGL to use
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	// 3. Then set our vertex attributes pointers
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0); //color according to position
	glEnableVertexAttribArray(1);
	//4. Unbind the VAO
	glBindVertexArray(0);
	// 5. (Game Loop) Draw the object in while
	return VAO;
}

GLuint mapTexture(const char* filename) {
	GLuint texture;
	int width, height, n;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	unsigned char* image = stbi_load(filename, &width, &height, &n, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image);
	glBindTexture(GL_TEXTURE_2D, 0);
	return texture;
}


GLuint createAxes(void) {
	GLuint VAO, VBO, EBO;
	GLfloat vertices[] = {
		0.0f, 0.0f, 0.0f, 1.0, 1.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0, 0.0f, 1.0f };
	GLuint indices[] = { 1,0, 2,0, 3,0};
	glGenVertexArrays(1, &VAO);
	// 1. Bind Vertex Array Object
	glBindVertexArray(VAO);
	// 2. Copy our vertices array in a buffer for OpenGL to use
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	// 3. Then set our vertex attributes pointers
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	//4. Unbind the VAO
	glBindVertexArray(0);
	// 5. (Game Loop) Draw the object in while
	return VAO;
}

GLuint createCubeVAO(void) {
	GLuint VAO, VBO, EBO;
	GLfloat vertices[] = {
		0.0f, 0.0f, 0.0f , 1.0f, 0.0f, 1.0f, // 0
		0.0f, 0.0f, 1.0f , 1.0f, 1.0f, 0.0f, // 1
		0.0f, 1.0f, 0.0f , 0.0f, 1.0f, 1.0f, // 2
		0.0f, 1.0f, 1.0f , 1.0f, 1.0f, 1.0f, // 3

		1.0f, 0.0f, 0.0f , 1.0f, 0.0f, 0.2f, // 4
		1.0f, 0.0f, 1.0f , 0.2f, 1.0f, 1.0f, // 5
		1.0f, 1.0f, 0.0f , 1.0f, 0.2f, 1.0f, // 6
		1.0f, 1.0f, 1.0f , 1.0f, 1.0f, 0.2f, // 7
	};
	GLuint indices[] = {
		0, 1, 3, 
		0, 2, 3, 

		0, 1, 5, 
		0, 4, 5,
		
		0, 2, 6,
		0, 4, 6,

		1, 3, 7,
		1, 5, 7,

		2, 3, 7,
		2, 6, 7,

		4, 5, 7,
		4, 6, 7
	};
	glGenVertexArrays(1, &VAO);
	// 1. Bind Vertex Array Object
	glBindVertexArray(VAO);
	// 2. Copy our vertices array in a buffer for OpenGL to use
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	// 3. Then set our vertex attributes pointers
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	//4. Unbind the VAO
	glBindVertexArray(0);
	// 5. (Game Loop) Draw the object in while
	return VAO;
}

void showFPS(void) {
	static double lastTime = glfwGetTime();
	static int nbFrames = 0;

	// Measure speed
	double currentTime = glfwGetTime();
	deltaTime = currentTime - lastTime;
	nbFrames++;
	if (currentTime - lastTime >= 1.0) { // If last prinf() was more than 1 sec ago
										 // printf and reset timer
		std::cout << 1000.0 / double(nbFrames) << " ms/frame -> " << nbFrames << " frames/sec" << std::endl;
		nbFrames = 0;
		lastTime += 1.0;
	}
}


static void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/)
{
	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;

	if (keys[GLFW_KEY_ESCAPE])
		glfwSetWindowShouldClose(window, GL_TRUE);

	// V-SYNC
	if (keys[GLFW_KEY_U]) {
		static bool vsync = true;
		if (vsync) {
			glfwSwapInterval(1);
		}
		else {
			glfwSwapInterval(0);
		}
		vsync = !vsync;
	}

	if ((keys[GLFW_KEY_0] || keys[GLFW_KEY_KP_0])) {
		std::cout << "You have pressed 0" << std::endl;
	}

}

static void mouse_button_callback(GLFWwindow* /*window*/, int button, int action, int /*mods*/) {
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_RIGHT] = true;
	else
		keys[GLFW_MOUSE_BUTTON_RIGHT] = false;

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_LEFT] = true;
	else
		keys[GLFW_MOUSE_BUTTON_LEFT] = false;

	if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_MIDDLE] = true;
	else
		keys[GLFW_MOUSE_BUTTON_MIDDLE] = false;
}

void Do_Movement() {
	if(keys[GLFW_KEY_W]) camera.ProcessKeyboard(FORWARD, deltaTime);
	if (keys[GLFW_KEY_S]) camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (keys[GLFW_KEY_A]) camera.ProcessKeyboard(LEFT, deltaTime);
	if (keys[GLFW_KEY_D]) camera.ProcessKeyboard(RIGHT, deltaTime);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (keys[GLFW_MOUSE_BUTTON_RIGHT]) {
		std::cout << "Mouse Position : (" << xpos << ", " << ypos << ")" << std::endl;
		if (firstMouse) {
			lastX = xpos;
			lastY = ypos;
			firstMouse = false;
		}
		GLfloat xoffset = xpos - lastX; GLfloat yoffset = ypos - lastY;
		lastX = xpos;
		lastY = ypos;
		camera.ProcessMouseMovement(xoffset, yoffset);

	}
}

void scroll_callback(GLFWwindow* window, double /*xoffset*/, double yoffset)
{
	if (keys[GLFW_MOUSE_BUTTON_LEFT]) {
		std::cout << "Mouse Offset : " << yoffset << std::endl;
	}
	camera.ProcessMouseScroll(yoffset);
}

